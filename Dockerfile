FROM python:3.7.15-slim

RUN pip install ansible && \
    apt update && apt install ssh -y && \
    mkdir -p ~/.ssh
